<?php

use App\Models\Category;
use App\Models\Question;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\QuestionsController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome', [
        'questions' => Question::latest()->paginate(6)->withQueryString(),
        'categories' => Category::all()
    ]);
});

Route::get('/tentang', function () {
    return view('about');
});


Route::resource('questions', QuestionsController::class)->middleware('auth');
Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::post('/logout', [LoginController::class, 'logout']);
Route::post('/komentar/{question_id}', [CommentController::class, 'store']);
Route::put('/komentar/{comentar_id}', [CommentController::class, 'update']);
Route::delete('/comment/{comentar_id}', [CommentController::class, 'destroy']);
Route::get('/comment/{comment_id}', [CommentController::class, 'index']);
// Route::get('/profile', [ProfileController::class, 'index'])->middleware('auth');;

Route::get('/register', [RegisterController::class, 'index']);
Route::post('/register', [RegisterController::class, 'store']);
Route::get('/category',function(){
    return view('category.views');
});
Route::get('categories/{category:id}', function(Category $category) {
    return view('category', [
        'questions' => $category->question,
        'category' => $category->name
    ]);
});
// Route::resource('category',CategoriesController::class);
// Route::get('/category',function(){
//     return view('category.views');
// });
Route::resource('category',CategoriesController::class);

Route::put('/profile/{id}',[ProfileController::class, 'update']);
Route::get('/profile',[ProfileController::class, 'index']);

Auth::routes();

