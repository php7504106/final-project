@extends('layouts.master')

@section('title')
    kategori
@endsection

@section('sub-title')
    Edit kategori
@endsection

@section('container')
  <form action="/category/{{$category->id}}" method="POST" enctype='multipart/form-data'>
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="exampleInputEmail1">Nama Kategori</label>
            <input type="text" class="form-control" name="name" value='{{$category->name}}'>
        </div>
        @error('name')
            <div class="alert alert-danger">{{$message }}</div>
        @enderror
        <div class="form-group">
            <label>Gambar Kategori</label>
            <div class="custom-file">
                <input type="file" name="image" class="form-control" id="image">
            </div>
        </div>
            @error('image')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        
        <button type="submit" class="btn btn-primary">Submit</button>
   </form>

@endsection