@extends('layouts.master')

@section('title')
    kategori
@endsection

@section('sub-title')
    Detail kategori
@endsection

@section('container')
  <div class="card">
  <img src="{{asset('/image/' .$category->image)}}" class="card-img-top" height='400px' alt="...">
  <div class="card-body">
    <h5 class="card-title">{{$category->name}}</h5>    
  </div>
</div>

<a href="/category" class="btn btn-light my-3">kembali</a>

@endsection