@extends('layouts.master')

@section('title')
    kategori
@endsection

@section('sub-title')
    Tambah kategori
@endsection

@section('container')
  <form action="/category" method="POST" enctype='multipart/form-data'>
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">Nama Kategori</label>
            <input type="text" class="form-control" name="name" placeholder="Masukan nama kategori">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Gambar Kategori</label>
            <div class="custom-file">
                <input type="file" name="image" class="form-control" id="image">
            </div>
        </div>
            @error('image')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        
        <button type="submit" class="btn btn-primary">Submit</button>
   </form>

@endsection