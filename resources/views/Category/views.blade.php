@extends('layouts.master')

@section('title')
    Categories
@endsection

@section('sub-title')
    view all categories
@endsection

@section('container')
   <a href='category/create' type="button" class="btn btn-primary my-3">Add category</a>



   <!-- DataTable with Hover -->
    <div class="col-lg-12">
        <div class="card mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">List of categories</h6>
            </div>
            <div class="table-responsive p-3">
            <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                <thead class="thead-light">
                    <tr>
                        <th>Category Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Category Name</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>

                    @forelse ($category as $key => $items)
                        <tr>
                            <td>{{Str::limit($items->name,25)}}</td>
                            <td><a href="/category/{{$items->id}}" class="btn btn-primary btn-sm mb-1">Detail</a> <a href="/category/{{$items->id}}/edit" class="btn btn-warning btn-sm mb-1">Edit</a>
                          
                                <form action = "/category/{{$items->id}}" method='post' class="d-inline">
                                @csrf
                                @method('delete')
                                <input type="submit" class="btn btn-danger btn-sm mb-1" value="Delete" />
                                </form>
                          
                            </td>
                        </tr>
                    @empty
                        <tr>
                    <td>
                        data kategori tidak ditemukan
                    </td>
                </tr>
                    @endforelse
                    </tbody>
            </table>
            </div>
        </div>
    </div>
@endsection