
@extends('layouts.master')

@section('title')
    <h2>All Questions</h2>
@endsection

@section('container')
    <div class="container">
        <div class="row mb-3">
            <a href="/questions/create" class="btn btn-primary"><i class="bi bi-plus"></i> Buat Pertanyaan</a>
            {{-- <div class="col-lg-3 ">
                <form action="/">
                    <div class="input-group">
                        <input type="text" class="form-control"  aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit">Search</button>
                        </div>
                    </div>
                </form>
            </div>    --}}
        </div>
        
        
    </div>
    
    <div class="container">
    </div>
        <div class="row">
           
            @forelse ($questions as $question)
            <div class="col-lg-4 my-3">
                <div class="card">
                    <div class="position-absolute px-2 py-1" style="background-color:rgba(0, 0, 0, 0.7)">
                        <a href="/categories/{{ $question->category->id }}" class="text-white text-decoration-none">{{ $question->category->name }}</a>
                    </div>
                    <div style="max-height: 300px; overflow:hidden">
                        <img class="card-img-top rounded" src="{{ asset('image/' . $question->image)}}" alt="Card image cap" width="300px">
                    
                    </div>
                    <div class="card-body">
                    <h5 class="card-title"><a href="/questions/{{ $question->id }}" class="text-decoration-none">{{ $question->judul }}</a></h5>
                    <small class="text-muted">
                        <p>By {{ $question->user->name }} <br> <i class="bi bi-clock"></i> 
                             {{ $question->created_at->diffForHumans() }}</p> 
                    </small>
                    <a href="/questions/{{ $question->id }}" class="btn btn-primary">Read..</a>
                    </div>

                    
                </div>
            </div>
                @empty
                    tidak ada data   
            @endforelse
            
       
    </div>
    <div class="d-flex justify-content-end">
        {{ $questions->links() }}
    
    </div>

@endsection