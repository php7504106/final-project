<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link href="{{ asset('/template/img/logo/logo.png') }}" rel="icon" />
    <title>Forum Diskusi</title>
    <link href="{{ asset('/template/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/template/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/template/css/ruang-admin.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/trix@2.0.0/dist/trix.css">
    <script type="text/javascript" src="https://unpkg.com/trix@2.0.0/dist/trix.umd.min.js"></script>
    <link href="{{ asset('/template/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
  </head>

  <body id="page-top">
    <div id="wrapper">
      <!-- Sidebar -->
      @include('partials.sidebar')
      <!-- Sidebar -->
      <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
          <!-- TopBar -->
         @include('partials.navbar')
          <!-- Topbar -->

          <!-- Container Fluid-->
          <div class="container-fluid">

          <!-- Page Heading -->
            {{-- <h1 class="h3 mb-4 text-gray-800">test judul('title')</h1> --}}
            <h1 class="h1 mb-4 text-gray-800">@yield('title')</h1>
          </div>
          <!-- /.container-fluid -->
          <div class=card-header>
              <h4 class="card-title">@yield('sub-title')</h4>
          </div>
          <div class='card-body'>
            {{-- <p> test kontent </p --}}
            @yield('container')
        </div>
          <!---Container Fluid-->
        </div>
        <!-- Footer -->
        <footer class="sticky-footer bg-white">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span
                >copyright &copy;
                <script>
                  document.write(new Date().getFullYear());
                </script>
                - developed by
                <b><a href="https://indrijunanda.gitlab.io/" target="_blank">Kelompok</a></b>
              </span>
            </div>
          </div>
        </footer>
        <!-- Footer -->
      </div>
    </div>

    <!-- Scroll to top -->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <script src="{{ asset('/template/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('/template/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/template/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('/template/js/ruang-admin.min.js') }}"></script>
    <script src="{{ asset('/template/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/template/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
      $(document).ready(function () {
        $('#dataTable').DataTable(); // ID From dataTable 
        $('#dataTableHover').DataTable(); // ID From dataTable with Hover
      });
    </script>

    {{-- <script src="{{asset('/template/js/Sweetalert.js')}}"></script> --}}
    {{-- <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.2/dist/sweetalert2.min.css" rel="stylesheet"> --}}
    @include('sweetalert::alert')
  </body>
</html>
