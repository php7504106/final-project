<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="img/logo/logo.png" rel="icon">
  <title>{{ $title }}</title>
  <link href="{{ asset('/template/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/template/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/template/css/ruang-admin.min.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">

</head>

<body class="bg-gradient-login">
  <!-- Login Content -->
  <div class="container-login">
    <div class="row justify-content-center">
      <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card shadow-sm my-5">
          <div class="card-body p-0">
            <div class="row">
              <div class="col-lg-12">
                <div class="login-form">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Register</h1>
                  </div>
                  <form action="/register" method="post">
                    @csrf
                    <div class="form-group">
                      <label>Name</label>
                      <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="exampleInputFirstName" >
                      @error('name')
                      <div id="validationServerUsernameFeedback" class="invalid-feedback">
                          {{ $message }}
                      </div>
                       @enderror
                    </div>
                    <div class="form-group">
                      <label>username</label>
                      <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" id="exampleInputFirstName" >
                      @error('username')
                      <div id="validationServerUsernameFeedback" class="invalid-feedback">
                          {{ $message }}
                      </div>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label>Email</label>
                      <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="exampleInputEmail" aria-describedby="emailHelp"
                        >
                        @error('email')
                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                    </div>
                    <div class="form-group">
                      <label>Password</label>
                      <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="exampleInputPassword" >
                      @error('password')
                      <div id="validationServerUsernameFeedback" class="invalid-feedback">
                          {{ $message }}
                      </div>
                       @enderror
                    </div>
                    {{-- <div class="form-group">
                      <label>Age</label>
                      <input type="text" class="form-control @error('age') is-invalid @enderror" name="age" id="exampleInputFirstName" >
                      @error('age')
                      <div id="validationServerUsernameFeedback" class="invalid-feedback">
                          {{ $message }}
                      </div>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label>Address</label>
                      <textarea class="form-control @error('adress') is-invalid @enderror" name="address" id="exampleFormControlTextarea1" rows="3"></textarea>
                      @error('address')
                      <div id="validationServerUsernameFeedback" class="invalid-feedback">
                          {{ $message }}
                      </div>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label>Bio</label>
                      <textarea class="form-control @error('bio') is-invalid @enderror" name="bio" id="exampleFormControlTextarea1" rows="3"></textarea>
                      @error('bio')
                      <div id="validationServerUsernameFeedback" class="invalid-feedback">
                          {{ $message }}
                      </div>
                      @enderror
                    </div> --}}
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-block">Register</button>
                    </div>
                    
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="font-weight-bold small" href="/login">Already have an account?</a>
                  </div>
                  <div class="text-center">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Login Content -->
  <script src="{{ asset('/template/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('/template/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('/template/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('/template/js/ruang-admin.min.js') }}"></script>
</body>

</html>