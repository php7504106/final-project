@extends('layouts.master')

@section('title')
<h2>Edit Data</h2>
@endsection

@section('container')
    <div class="row">
        <div class="col-lg-8">
            <form class="form-group" action="/questions/{{ $question->id }}" method="post" enctype="multipart/form-data">
                @method('put')
                @csrf
                <div class="mb-3">
                  <label  class="form-label">Judul</label>
                  <input type="text" class="form-control @error('judul') is-invalid @enderror" name="judul" value="{{ $question->judul }}">
                </div>
                <div class="mb-3">
                    <label  class="form-label">Isi</label>
                    <input id="x" type="hidden" name="isi" value="{{ $question->isi }}">
                    <trix-editor input="x"></trix-editor>
                </div>
                <div class="mb-3">
                    <label for="slug" class="form-label">Category : </label>
                    <select class="form-select" aria-label="Default select example" name="category_id" >
                      @foreach ($categories as $item)
                        @if ($question->category_id == $item->id)
                            <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                        @else 
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endif
                        
                      @endforeach
                    </select>
                </div>


                <div class="form-group">
                    <div class="mb-3">
                        <label for="formFile" class="form-label">Image</label>
                        @if ($question->image)
                        <img src="{{ asset('image/' . $question->image) }}"  class="img-preview img-fluid mb-3 col-sm-5 d-block">
                        @else
                        <img  class="img-preview img-fluid mb-3 col-sm-5">
                        @endif
                       
                        <input class="form-control" type="file" name="image" id="image" onchange="previewImage()">
                    </div>
                    @error('image')
                        <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Edit Data</button>
              </form>
        </div>
    </div>

    <script>

        document.addEventListener('trix-file-accept', function(e) {
            e.preventDefault();
        })

        function previewImage() {
            
            const image = document.querySelector('#image');
            const imgPreview = document.querySelector('.img-preview');

            imgPreview.style.display = 'block';

            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);

            oFReader.onload = function(oFREvent) {
                imgPreview.src = oFREvent.target.result;
            }
        }
    </script>
@endsection