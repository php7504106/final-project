@extends('layouts.master')

@section('title')
    <h2>Pengelolaan Pertanyaaan</h2>
@endsection

@section('container')
    <div class="row">
        <div class="col-lg-12">
            <div class="card mb-4">
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <a href="/questions/create" class="btn btn-primary"><i class="bi bi-plus"></i> Tambah Pertanyaan</a>
              </div>
              <div class="table-responsive p-3">
                <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                  <thead class="thead-light">
                    <tr>
                      <th>Judul</th>
                      <th>Category</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Judul</th>
                      <th>Category</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @forelse ($questions as $question)
                    <tr>
                      <td>{{ $question->judul }}</td>
                      <td>{{ $question->category->name }}</td>
                      <td>
                        <a href="/questions/{{ $question->id }}" class="btn btn-primary btn-sm">Detail</a>
                        <a href="/questions/{{ $question->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <form action="/questions/{{ $question->id }}" method="post" class="d-inline">
                          @csrf
                          @method('delete')
                          <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">Delete</button>
                        </form>
                      </td>
                    </tr>
                    @empty
                        <h4>belum ada data</h4>
                    @endforelse
                    
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
    </div>
@endsection