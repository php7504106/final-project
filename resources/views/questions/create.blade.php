@extends('layouts.master')

@section('title')
<h2>Tambah Data</h2>
@endsection

@section('container')
    <div class="row">
        <div class="col-lg-8">
            <form class="form-group" action="/questions" method="post" enctype="multipart/form-data">
                @csrf
                <div class="mb-3">
                  <label  class="form-label">Judul</label>
                  <input type="text" class="form-control @error('judul') is-invalid @enderror" name="judul">
                  @error('judul')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
              @enderror
                </div>
                <div class="mb-3">
                    <label  class="form-label @error('isi') is-invalid @enderror">Isi</label>
                    <input id="x" type="hidden" name="isi">
                    <trix-editor input="x"></trix-editor>
                @error('isi')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
                </div>
                <div class="mb-3">
                    <label for="slug" class="form-label">Category : </label>
                    <select class="form-select" aria-label="Default select example" name="category_id">
                      @foreach ($categories as $item)
                          <option value="{{ $item->id }}">{{ $item->name }}</option>
                      @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <div class="mb-3">
                        <label for="formFile" class="form-label">Image</label>
                        <img  class="img-preview img-fluid mb-3 col-sm-5">
                        <input class="form-control" type="file" name="image" id="image" onchange="previewImage()">
                    </div>
                    @error('image')
                        <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah Data</button>
              </form>
        </div>
    </div>

    <script>

        document.addEventListener('trix-file-accept', function(e) {
            e.preventDefault();
        })
        function previewImage() {
            
            const image = document.querySelector('#image');
            const imgPreview = document.querySelector('.img-preview');

            imgPreview.style.display = 'block';

            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);

            oFReader.onload = function(oFREvent) {
                imgPreview.src = oFREvent.target.result;
            }
        }

    </script>
@endsection