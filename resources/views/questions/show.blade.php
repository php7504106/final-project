@extends('layouts.master')

@section('title')
    <h2>{{ $question->judul }}</h2>
@endsection

@section('container')
    <div class="container">
        
        <div class="row">
            <div class="col-lg-8">
                <a href="/questions" class="btn btn-primary btn-sm mb-3"><i class="bi bi-arrow-left"></i> Kembali</a>
                <img src="{{ asset('image/' . $question->image) }}" class="img-fluid mt-2" alt="...">
                <article class="my-3">
                    <p>{!! $question->isi !!}</p>
                </article>
            </div>
            <div class="col-lg-4">
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action active">
                    Daftar Kategori
                </a>
                @foreach ($categories as $item)
                <a href="/categories/{{$item->id }}" class="list-group-item list-group-item-action">{{$item->name}}</a>
                    
                @endforeach
                 </div>
            </div>
        
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-11">
                <hr>
                <h2>List Comments ({{  $question->comment->count()  }})</h2>
                <hr>
                @forelse ($question->comment as $item)
                
                <div class="media my-4">
                    <img src="{{ asset('/template/img/boy.png') }}" class=" img-profile rounded-circle" style="max-width: 40px" alt="...">
                    <div class="media-body">
                        <div class="d-flex flex-row bd-highlight">
                            <div class="p-2 bd-highlight"> <h5 >{{ $item->user->name }}</h5></div>
                            @if ($item->user->id == auth()->user()->id)
                                <div class="p-1 bd-highlight" ><a href="/comment/{{ $item->id }}" class="btn btn-warning btn-sm"><i class="bi bi-pencil-square"></i></a></div>
                        
                                <div class="p-1 bd-highlight">
                                    <form action="/comment/{{ $item->id }}" method="post" class="d-inline">
                                        @csrf
                                        @method('delete')
                                        <input type="hidden" name="question_id" value="{{ $item->question_id }}">
                                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="bi bi-trash"></i></button>
                                      </form>
                                    
                                </div>
                            @endif
                          </div> 
                      <p>{!! $item->comment !!}</p>
                      <small class="text-muted">
                         <i class="bi bi-clock"></i> 
                         {{ $item->created_at->diffForHumans() }}</p> 
                    </small>
                    </div>
                  </div>
                @empty
                    <p>belum ada data komentar</p>
                @endforelse
                <form action="/komentar/{{ $question->id }}" method="post">
                    @csrf
                    <div class="mb-3">
                        <input id="x" type="hidden" name="comment">
                        <trix-editor input="x"></trix-editor>
                        @error('comment')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Tambah Komentar</button>
                </form>
            </div>
        </div>
    </div>
@endsection