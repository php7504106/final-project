@extends('layouts.master')
@section('title')
Halaman edit profil
    
@endsection
@section('sub-title')
    profile
@endsection
@section('container')
<form action="/profile/{{$profile->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
        <label >bio</label>
        <textarea name="bio" class="form-control" cols="30" rows="10">{{$profile->bio}}</textarea>
      </div>
      @error('bio')
      <div class="alert alert-danger">{{$message}}</div>
          
      @enderror
    <div class="form-group">
      <label >age</label>
      <input type="number" name="age" class="form-control" value="{{$profile->age}}">
      
    </div>
    @error('age')
    <div class="alert alert-danger">{{$message}}</div>
        
    @enderror

    <div class="form-group">
        <label>address</label>
        <textarea name="address" class="form-control" cols="30" rows="10">{{$profile->address}}</textarea>  
      </div>
      @error('address')
      <div class="alert alert-danger">{{$message}}</div>
          
      @enderror
      {{-- <div class="form-group">
        <label>user id</label>
        <input type="text" name="user_id" class="form-control">
      </div>
      @error('nama')
      <div class="alert alert-danger">{{$message}}</div>
          
      @enderror --}}
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/" class="btn btn-dark"> Cancel</a>
  </form>
@endsection