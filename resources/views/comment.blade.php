@extends('layouts.master')

@section('title')
    <h2>Edit Komentar</h2>
@endsection

@section('container')
    <div class="container">
        <div class="row mb-3">
            <form action="/komentar/{{ $komentar->id }}" method="post">
                @csrf
                @method('put')
                <div class="mb-3">
                    <input type="hidden" name="question_id" value="{{ $komentar->question_id }}">
                    <input id="x" type="hidden" name="comment">
                    <trix-editor input="x">{!! $komentar->comment !!}</trix-editor>
                    @error('comment')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary btn-sm">Update</button>
            </form>
            
        </div>
        
        
    </div>
    
    
@endsection