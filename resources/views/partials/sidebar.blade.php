<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
      <div class="sidebar-brand-icon">
        <img src="{{ asset('/template/img/logo/logo2.png') }}" />
      </div>
      <div class="sidebar-brand-text mx-3">RuangTanya</div>
    </a>
    @auth
    <hr class="sidebar-divider my-0" />
    <li class="nav-item">
      <a class="nav-link" href="/">
        <i class="bi bi-house"></i>
        <span>Home</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/questions">
          <i class="bi bi-question-circle"></i>
        <span>Pertanyaan</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/category">
          <i class="bi bi-tags"></i>
        <span>Kategori</span></a>
    </li>
    @else
    <hr class="sidebar-divider my-0" />
    <li class="nav-item">
      <a class="nav-link" href="/">
        <i class="bi bi-house"></i>
        <span>Home</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/tentang">
          <i class="bi bi-question-circle"></i>
        <span>Tentang</span></a>
    </li>
    @endauth
    
</ul>