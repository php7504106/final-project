
@extends('layouts.master')

@section('title')
    <h2>Questions By {{ $category }}</h2>
@endsection

@section('container')
    <div class="container">
        <div class="row mb-4">
            <a href="/questions/create" class="btn btn-primary"><i class="bi bi-plus"></i> Buat Pertanyaan</a>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            @forelse ($questions as $question)
            <div class="col-lg-4 my-3">
                <div class="card">
                    <div class="position-absolute px-2 py-1" style="background-color:rgba(0, 0, 0, 0.7)">
                        <a href="/categories/{{ $question->category->id }}" class="text-white text-decoration-none">{{ $question->category->name }}</a>
                    </div>
                    <div style="max-height: 300px; overflow:hidden">
                        <img class="card-img-top rounded" src="{{ asset('image/' . $question->image)}}" alt="Card image cap" width="300px">
                    
                    </div>
                    <div class="card-body">
                    <h5 class="card-title"><a href="/questions/{{ $question->id }}" class="text-decoration-none">{{ $question->judul }}</a></h5>
                    <small class="text-muted">
                        <p>By {{ $question->user->name }} <br> <i class="bi bi-clock"></i> 
                             {{ $question->created_at->diffForHumans() }}</p> 
                    </small>
                    <a href="/questions/{{ $question->id }}" class="btn btn-primary">Read..</a>
                    </div>
                </div>
            </div>
                @empty
                    tidak ada data
                
            
            @endforelse
        
        </div>
    </div>

@endsection