<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
</p>

## Tugas akhir kelas laravel 

Web ini adalah bagian dari rangkaian kelas bootcamp laravel yang diselenggarakan oleh sanbercode di batch 42


## Kelompok 14
### anggota Kelompok
1. Agung Rizqy Wiryawan
2. Andika Bagus Wicaksono
3. Akmal Muzakkir

## Tema project
Forum Diskusi

## ERD

<img src="https://drive.google.com/uc?export/view&id=1FK07TW0tDCtjC1L72fzem87kNytaVni1">

## Demo appication

Link Video demo ada [disini](https://www.youtube.com/embed/yVtheRpM0wg)

## link Deployment

**http://ruangtanya.sanbercodeapp.com/**

## Used Package

- [ ] Template : RuangAdmin

- [ ] Library : trix, sweet alert, datatables
