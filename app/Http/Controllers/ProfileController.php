<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;


class ProfileController extends Controller
{
    public function index(){
        $idUser = Auth::id();
        $profile = Profile::where('user_id', $idUser)->first();
        return view('profile.edit',['profile' => $profile]);
    }
    public function update($id, Request $request){
        $request->validate([
            'bio' => 'required',  
            'age' => 'required',
            'address' => 'required',        
        ]);
        Profile::where('id', $id)
        ->update(
            [
            'bio' => $request ['bio'],
            'age' => $request ['age'],
            'address' => $request ['address'],
            ]
        );
        Alert::success('Berhasil', 'Profile berhasil diupdate.');
        return redirect('/');
    }
}


