<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class CommentController extends Controller
{
    public function index($id)
    {
        $comment = DB::table('comments')->find($id);

        return view('comment', [
            'komentar' => $comment
        ]);
    }
    public function store(Request $request, $id) 
    {
        $request->validate([
            'comment' => 'required'
        ]);

        $user_id = Auth::id();

        $comment = new Comment;

        $comment->comment = $request->comment;
        $comment->user_id = $user_id;
        $comment->question_id = $id;

        $comment->save();
        Alert::success('Berhasil', 'Komentar berhasil ditambahkan');
        return redirect('/questions/' . $id);

    }

    public function update(Request $request, $id) 
    {

        DB::table('comments')
              ->where('id', $id)
              ->update(['comment' => $request['comment']]);

        $question_id = $request['question_id'];
        Alert::success('Berhasil', 'Komentar  berhasil diedit');
        return redirect('/questions/'. $question_id);
    }

    public function destroy(Request $request, $id)
    {
        DB::table('comments')->where('id', '=', $id)->delete();

        $question_id = $request['question_id'];
        Alert::success('Berhasil', 'Komentar  berhasil dihapus');
        return redirect('/questions/'. $question_id);
    }
}
