<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use File;
use RealRashid\SweetAlert\Facades\Alert;


class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // return 'Berhasil';
        $category = Category::all();
        return view('category.views', ['category' => $category]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('category.tambah');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $request->validate([
            'name' => 'required',
            'image' => 'required|mimes:png,jgp,jpeg|max:2048',
           ]);
           
           //dd($request->all());
           $imageName= time().'.'.$request->image->extension();
           $request->image->move(public_path('image'), $imageName);

           $category = new Category;
 
            $category->name = $request["name"];
            $category->image = $imageName;
            $category->save();

            Alert::success('Berhasil', 'Category berhasil ditambahkan');


            return redirect('/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $category=Category::find($id);
        //  dd($category);
        return view('Category.detail',['category'=>$category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category=Category::find($id);
        return view('Category.edit',['category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // dd($request);
        $request->validate([
            'name' => 'required',
            'image' => 'mimes:png,jgp,jpeg|max:2048',
           ]);
           
           //dd($request->all());
           
           $category = Category::find($id);
           $category->name=$request->name;
           if ($request->has('image')) {
                $path = 'image/';
                File::delete($path.$category->image);
                $newImage=  time().'.'.$request->image->extension();
                $request->image->move(public_path('image'), $newImage);
                $category->image=$newImage;

           }
        //    dd($category);
           $category-> save();

           Alert::success('Berhasil', 'Category berhasil di update');


           return redirect('/category');
        }       
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $category = Category::find($id);
        $path = 'image/';
        File::delete($path.$category->image);
        $category->delete();
        Alert::success('Berhasil', 'Category berhasil dihapus.');

        return redirect('/category');
    }
}
