<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Question;
use Illuminate\Http\Request;
use File;
use RealRashid\SweetAlert\Facades\Alert;



class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('questions.index', [
            'questions' => Question::where('user_id', auth()->user()->id)->get()
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('questions.create', [
            'categories' => Category::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|mimes:png,jpg,jpeg|max:2048',
            'judul' => 'required',
            'isi' => 'required',
            'category_id' => 'required'
    
        ]);

        $fileName = time().'.'.$request->image->extension(); 
        
        $request->image->move(public_path('image'), $fileName);
        
        $question = new Question;
 
        $question->judul = $request->judul;
        $question->isi = $request->isi;
        $question->category_id = $request->category_id;
        $question->image =  $fileName;
        $question->user_id = auth()->user()->id;

 
        $question->save();
        Alert::success('Berhasil', 'Pertanyaan berhasil ditambahkan');

        return redirect('/questions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        
        return view('questions.show', [
            'question' => $question,           
            'categories' => Category::all()
            
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        
        return view('questions.edit', [
            'question' => $question,
            'categories' => Category::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'image' => 'mimes:png,jpg,jpeg|max:2048',
            'judul' => 'required',
            'isi' => 'required',
            'category_id' => 'required'
    
        ]);

        $question = Question::find($id);
 
        $question->judul = $request->judul;
        $question->isi = $request->isi;
        $question->category_id = $request->category_id;
        
        
        if ($request->has('image')) {
            $path = 'image/';
            File::delete($path. $question->image);
            $newImage=  time().'.'.$request->image->extension();
            $request->image->move(public_path('image'), $newImage);
            $question->image=$newImage;

        }
        
        $question->save();
        Alert::success('Berhasil', 'Pertanyaan berhasil di update');


        return redirect('/questions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);
        $path = 'image/';
        File::delete($path.$question->image);
        $question->delete();
        Alert::success('Berhasil', 'Category berhasil dihapus.');

        return redirect('/questions');
        // Question::destroy($question->id);
        // Alert::success('Berhasil', 'Pertanyaan berhasil di hapus');
        // return redirect('/questions')->with('success', 'Post has been deleted!');
    }
}
